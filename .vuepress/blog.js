// blog-style patch (modified)
// https://github.com/ycmjason/ycmjason.github.io/blob/dev/.vuepress/config.js

const { join, basename, relative } = require('path');
const moment = require('moment');
const glob = require('glob');
const { readFileSync } = require('fs');

const readTitleFromMd = path => {
  try {
    const match = readFileSync(path, 'utf8').trim().match(/# (.*)/);
    if (!match) return;
    return match[1].trim();
  } catch (e) {
    return ''
  }
};

module.exports.generateBlogSideBar = dir => {
  const structure = {};
  glob.sync('**/*.md', { cwd: join(__dirname, '..', dir) })
    .filter(p => basename(p) !== 'README.md')
    .map(p => {
      const [year, month, day, filename] = p.split('/');
      return [year, month, day, filename];
    }).forEach(([year, month, day, filename]) => {
      structure[year] = { ...structure[year] };
      structure[year][month] = { ...structure[year][month] };
      structure[year][month][day] = {
        ...structure[year][month][day],
        [filename]: join(dir, year, month, day, filename),
      };
    });

  const years = Object.keys(structure).sort().reverse();
  let i = 0;
  return [].concat(...years.map(year => {
    const months = Object.keys(structure[year]).sort().reverse();
    return months.map(month => {
      i++;
      const days = Object.keys(structure[year][month]).sort().reverse();
      const title = moment(`${year}-${month}`).format('YYYY MMM')
      const collapsable = i === 1 ? false : !(year + month === Math.max(...years) + Math.max(...months))
      const children = [].concat(...days.map(day => {
        const files = Object.keys(structure[year][month][day]).sort().reverse();
        return files.map(file => {
          const url = join(dir, year, month, day, file);
          const date = moment(`${year}-${month}-${day}`).format('M/DD');
          const title = readTitleFromMd(join(__dirname, '..', url))
          return [url, date + ' - ' + title]
        })
      }))
      return {title, collapsable, children};
    });
  }));
};
