const {generateBlogSideBar} = require('./blog')

module.exports = {
  title: '電池冷蔵庫',
  description: '飲んでも飲まなくても震える',
  ga: 'UA-88361167-5',
  head: [
    ['meta', { name: 'og:title', content: '電池冷蔵庫' }],
    ['meta', { name: 'twitter:card', content: 'summary' }],
    ['meta', { name: 'twitter:site', content: '@tottokotkd' }],
    ['meta', { name: 'twitter:creator', content: '@tottokotkd' }],
  ],
  themeConfig: {
    nav: [
      { text: 'notes', link: '/notes/' },
      { text: 'privacy policy', link: '/privacy.md' },
      { text: 'twitter', link: 'https://twitter.com/tottokotkd' },
    ],
    sidebar: {
      '/notes/': [
        '',
        ...generateBlogSideBar('/notes'),
      ],
    },
    sidebarDepth: 2
  },
  markdown: {
    config: md => {
      md.use(require('markdown-it-footnote'))
    }
  }
}
