# またブログを作ってしまった [VuePress]

「また」なんだ。すまない。

::: warning 御注意
VuePressは多分あっという間に進歩するので、この記事をいつまでも参考にしないでね。
:::

## VuePress でブログ風サイトを頑張る

「VuePressのブログテーマは未完成のはずでは？」という程度に話が通じる人に向けて書きます[^1]。

VuePressのブログ関連機能はまだ存在しません。単に未完成であるばかりか「タグ付けたい」だの、「RSS吐きたい」だの、欲張りさんですね。相当先まで完成しない臭いが漂っています。RSSはまだしも、タグ云々やりたい人はGatsbyでGraphQLを書けば幸せなので、VuePressくんがいきなりそこまで無理しなくてもいいのでは… などと思わないでもないです。

いずれにせよ、VuePressでブログを作りたければ自力で何とかするしかなさそうです。詳しい話は「[VuePress をお試ししてみた](https://blog.dojineko.ninja/2018/04/15/try-vuepress/)」を読もう！

しかし、自力で何とかと言いつつも「既製品が完成するまで全部人力」となると流石に厳しすぎますね。贅沢は言わないので、ひとまず「マークダウンを置くとサイドバーに出てくる」くらいの機能があれば当面は凌げそうです。なので作りました。

…「作りました」という態度で延々偉そうなことを書いてもいいんですが、実のところ[例示されたコード](https://github.com/vuejs/vuepress/issues/36#issuecomment-383281874)をパクって改造しただけなので、それを貼っておきます[^2]。

```js
// .vuepress/config.js

const {generateBlogSideBar} = require('./blog')

module.exports = {
  title: '電池冷蔵庫',
  description: '飲んでも飲まなくても震える',
  themeConfig: {
    nav: [
      { text: 'Home', link: '/' },
      { text: 'Twitter', link: 'https://twitter.com/tottokotkd' },
    ],
    sidebar: {
      '/notes/': [
        '',
        ...generateBlogSideBar('/notes'),
      ],
    },
    sidebarDepth: 2
  },
  markdown: {
    config: md => {
      md.use(require('markdown-it-footnote'))
    }
  }
}
```

```js
//  .vuepress/blog.js
// blog-style patch (modified)
// https://github.com/ycmjason/ycmjason.github.io/blob/dev/.vuepress/config.js

const { join, basename, relative } = require('path');
const moment = require('moment');
const glob = require('glob');
const { readFileSync } = require('fs');

const readTitleFromMd = path => {
  try {
    const match = readFileSync(path, 'utf8').trim().match(/^#(.*)/);
    if (!match) return;
    return match[1].trim();
  } catch (e) {
    return ''
  }
};

module.exports.generateBlogSideBar = dir => {
  const structure = {};
  glob.sync('**/*.md', { cwd: join(__dirname, '..', dir) })
    .filter(p => basename(p) !== 'README.md')
    .map(p => {
      const [year, month, day, filename] = p.split('/');
      return [year, month, day, filename];
    }).forEach(([year, month, day, filename]) => {
      structure[year] = { ...structure[year] };
      structure[year][month] = { ...structure[year][month] };
      structure[year][month][day] = {
        ...structure[year][month][day],
        [filename]: join(dir, year, month, day, filename),
      };
    });

  const years = Object.keys(structure).sort().reverse();
  let i = 1;
  return [].concat(...years.map(year => {
    const months = Object.keys(structure[year]).sort().reverse();
    return months.map(month => {
      i++;
      const days = Object.keys(structure[year][month]).sort().reverse();
      const title = moment(`${year}-${month}`).format('YYYY MMM')
      const collapsable = i === 1 ? false : !(year + month === Math.max(...years) + Math.max(...months))
      const children = [].concat(...days.map(day => {
        const files = Object.keys(structure[year][month][day]).sort().reverse();
        return files.map(file => {
          const url = join(dir, year, month, day, file);
          const date = moment(`${year}-${month}-${day}`).format('M/DD');
          const title = readTitleFromMd(join(__dirname, '..', url))
          return [url, date + ' - ' + title]
        })
      }))
      return {title, collapsable, children};
    });
  }));
};
```

ディレクトリ構成はこんな感じ。

```
.vuepress
  public
  blog.js
  config.js
  override.styl
node_modules
notes
  README.md
  2018
    04
      23
        blog.md
.editorconfig
.gitignore
.gitlab-ci.yml
index.md
package.json
package-lock.json
```

改造前との違いは、マークダウンのファイル名がURLの一部になるだけです。
`http://example.com/2000/12/31.md` というパスはちょっと辛いと感じたので、そこだけ改造。

## 今後の件

このサイトは適当にブログをやっていきます。VuePressちゃんカワイイ。

VuePressリポジトリのブログ関連作業が仕上がったら使ってみたいですね。ただ、自分がPRを出す時間はないので全面的フリーライド…　そこは今後ちょっと考えます。

欲しい機能リスト:

* タイムスタンプくらい出したい
* `og:title` 入れたい


[^1]: [Blog Support roadmap #36](https://github.com/vuejs/vuepress/issues/36)


[^2]: ライセンスはMITでいいのかな。この程度の改造コード片では二次的著作物に当たらないような気もしますが、まあ何でもいいです。
