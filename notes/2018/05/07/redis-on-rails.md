---
meta:
  - name: og:title
    content: RailsからRedisを使うときにinitializerは必要ない件
  - name: description
    content: なんか不思議なサンプルコードが多いけど実際は要らない（ぽい）
---

# RailsからRedisを使うときにinitializerは必要ない件


要するに環境変数 `REDIS_URL` を設定しておけばオッケー。

1. `gem 'redis', '~> 4.0'`
2. `REDIS_URL=redis://localhost:36379 rails c`
3. `Redis.current`

これで使える。ぽい。

```ruby
Redis.current
# => #<Redis client v4.0.1 for redis://localhost:36379/0>

Redis.current.set :hoge, 1000
# => "OK"

Redis.current.get :hoge
# => "1000"
```

個人的に環境変数まわりはdirenvがお気に入りですが、動けば.bashrcでも.zshrcでも何でもいいです。

ちなみに、いま作業中のプロジェクトが持つ `.envrc` は現状こんな感じ。

```bash
# rbenv
eval "$(rbenv init -)"
echo -n "ebenv local: Ruby " && rbenv local

# MySQL
docker run -d --rm --name tgb-db -p 33306:3306 -e MYSQL_ROOT_PASSWORD=password -e MYSQL_DATABASE=tgb_dev mysql:5.7 --character-set-server=utf8mb4 --collation-server=utf8mb4_unicode_ci 2> /dev/null || true
docker run -d --rm --name tgb-db-test -p 33307:3306 -e MYSQL_ROOT_PASSWORD=password -e MYSQL_DATABASE=tgb_test mysql:5.7 --character-set-server=utf8mb4 --collation-server=utf8mb4_unicode_ci 2> /dev/null || true
# docker rm -f tgb-db tgb-db-test

export DB_HOST=127.0.0.1
export DB_DATABASE=tgb_dev
export DB_PORT=33306
export DB_USERNAME=root
export DB_PASSWORD=password

export DB_TEST_HOST=127.0.0.1
export DB_TEST_DATABASE=tgb_test
export DB_TEST_PORT=33307
export DB_TEST_USERNAME=root
export DB_TEST_PASSWORD=password

# redis
docker run -d --rm --name tgb-redis -p 36379:6379 redis 2> /dev/null || true
export REDIS_URL=redis://localhost:36379

# node
PATH_add client/node_modules/.bin

# gcp sdk
PATH_add tmp/google-cloud-sdk/bin
```

RubyMineからコンソールを開くだけでインフラの用意が完了するので大変つよい。
