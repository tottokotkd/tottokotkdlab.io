---
meta:
  - name: og:title
    content: Node v10では裸のawaitが使えてFirebase Admin APIも楽
  - name: description
    content: やったぜ非同期！
---

# Node v10では裸のawaitが使えてFirebase Admin APIも楽

これで起動して

```bash
$ node --version
v10.3.0

$ node --experimental-repl-await
```

こうじゃ

```js
// node install は適当にやっておく
const admin = require("firebase-admin")

// firebaseコンソール > 設定 > サービスアカウント > Admin SDK 秘密鍵生成
// 最強のキーなので間違ってもGitHubに公開してはいけない
const serviceAccount = require("./auth.json")

// 秘密鍵生成するところに書いてあるので、それをコピペ
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://[[PROJECT_NAME]].firebaseio.com"
})

// async / await できるぞ！
await admin.firestore().collection('/users').doc('userID').set({name: 'hoge'})
const userDoc = await admin.firestore().collection('/users').doc('userID').get()
console.log('user', userDoc.data())
await admin.firestore().collection('/users').doc('userID').delete()
```

やったぜ！
