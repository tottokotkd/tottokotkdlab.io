---
meta:
  - name: og:title
    content: 知人以上パートナー未満の人がほしい
  - name: og:description
    content: 知人以上って友達募集じゃん辛っ / いやでも真面目な話ですからこれ
---

# 知人以上パートナー未満の人がほしい

## 結論だけ言え

* 最近とてもさみしい
* 程よく都合のいい人間関係がほしい
* どっか出かけたりとか、なんか食べたりとか、そういうちょっとしたやつで
* いつか誰かと結婚するかもしれないけど、これは婚活ではないです

## お前誰

* 日本語で思考して日本語で会話する29歳男性
* 9月中に転居して、10月から東京でITエンジニアとして稼働予定
* 旅行や外食でだらだら時間を使うのが好き、あとはweb開発も好きで毎日やってる
* 好きな街は圧倒的にウィーン、パルテッレで立ち見とか最高（次点は那覇）
* 酒と煙草は（楽しめるけど）今はどちらもやらない
* 性自認は男性、性指向は基本女性
* 今より10kgほど痩せていた頃「シベリア流刑時代のトロツキーに似ている」と言われた経験あり

<p><a href="https://commons.wikimedia.org/wiki/File:Leo_Trotzki_1900_in_Sibirien.jpg#/media/File:Leo_Trotzki_1900_in_Sibirien.jpg"><img src="https://upload.wikimedia.org/wikipedia/commons/c/c6/Leo_Trotzki_1900_in_Sibirien.jpg" alt="Leo Trotzki 1900 in Sibirien.jpg"></a><br>By <span lang="ja">不明</span> - <a rel="nofollow" class="external free" href="http://www.marxists.org/archive/trotsky/photo/t1900c.htm">http://www.marxists.org/archive/trotsky/photo/t1900c.htm</a>, パブリック・ドメイン, <a href="https://commons.wikimedia.org/w/index.php?curid=321947">Link</a></p>

## こんな人が好き

* 自分で考える人
* 誤解を放置せずに細かく反論訂正してくれる人
* 美術館で3時間とか潰して平然としている人

## こういう人は嫌い

* 調べよう、知ろうとしない人
* 自分以外に興味がない人
* フレグランスの使い方があまりにも過激な人

## 婚活なの？

* 婚活ではないです
* 仮に結婚することがあっても、子供は絶対に作らないと決めているので、子供が欲しい人はダメです
* でも結婚の税制優遇っていいよね… (現実逃避)
* でかい部屋を借りて二人暮らしで家電調達とかいいよね… (現実逃避)
* 二馬力で稼いで貯めて、いざという時に安心だといいよね… (現実逃避)
* 一応書いておくと、あるか無いかで言えば性欲はある
* ただ人生観により子供は作らないし、セックスもしないと決めているだけ。宗教ではないけど、そんな感じの認識でも大差ないです
* こういう前提で新しい人間関係がほしい
* 婚活ではないです

## 最後に

* これは本当に真面目な記事です。信じて
* とりあえずなんか食いに行こうぜ！ 
* ツイッターのDMで誘ってあげてください
